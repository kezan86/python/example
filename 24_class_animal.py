#!/usr/bin/python3
class Animal():

    sound = ''

    def __init__(self, name, weight):
        self.name = name
        self.weight = weight

    def say(self):
        print(self.sound)

    def feeding(self):
        print('Вы покормили', self.name)

class Bird(Animal):
    def collect_eggs(self):
        print('Вы собрали яйца у ', self.name)

class Mammal(Animal):  # млекопитающие
    def milk(self):
        print('Вы подоили', self.name)

class Sheep(Animal):
    sound = 'бе бе'

    def shearing(self):
        print('Вы постригли', self.name)

class Goose(Bird):
    sound = 'га га'

class Cow(Mammal):
    sound = 'му му'

class Chicken(Bird):
    sound = 'ко ко'

class Goat(Mammal):
    sound = 'бе бе'

class Duck(Bird):
    sound = 'кря кря'

def anims(*animals):
    pass

def max_weight(animals):
    max_weight = 0
    al_weight = 0
    name_max_weight = ''
    for type, animal_name in animals.items():
        for item in range(len(animal_name)):
            al_weight += animal_name[item].weight
            if max_weight <= animal_name[item].weight:
                max_weight = animal_name[item].weight
                name_max_weight = animal_name[item].name
    print(f'Максимальный вес животных - {al_weight} кг.')
    print(f'Имя самого большого животного {name_max_weight} и вес его равен: {max_weight} кг.')

def monuals_animals(animals):
    for type, animal_name in animals.items():
        for item in range(len(animal_name)):
            animal_name[item].feeding()
            animal_name[item].say()
            if 'Bird' in type.__name__:
                animal_name[item].collect_eggs()
            if 'Mammal' in type.__name__:
                animal_name[item].milk()
            if 'Sheep' in type.__name__:
                animal_name[item].shearing()

def main():
    grey_goose = Goose('Серый', 15)
    white_goose = Goose('Белый', 12)
    cow = Cow('Манька', 120)
    barashek = Sheep('Барашек', 59)
    kudryash = Sheep('Кудрявый', 65)
    koko_hen = Chicken('Ко-Ко', 8)
    kuka_hen = Chicken('Кукареку', 5)
    goat_0 = Goat('Рога', 37)
    goat_1 = Goat('Копыта', 38)
    duck = Duck('Кряква', 19)

    animals = {Mammal: [cow, goat_0, goat_1], Sheep: [barashek, kudryash], Bird: [grey_goose, white_goose, koko_hen, kuka_hen, duck]}

    monuals_animals(animals)
    max_weight(animals)

if __name__ == '__main__':
    main()