#!/usr/bin/python3

if __name__ == '__main__':
    side_sled = int(input("Введите длину стороны квадрата: "))
    perimeter_square = side_sled * 4
    square_area = side_sled * side_sled
    print("Вывод:")
    print("Периметр: ", perimeter_square)
    print("Площадь: ", square_area)
    length_rectangle = int(input("Введите длину прямоугольника: "))
    width_rectangle = int(input("Введите ширину прямоугольника: "))
    perimeter_rectange = (length_rectangle + width_rectangle) * 2
    square_rectange = length_rectangle * width_rectangle
    print("Вывод:")
    print("Периметр: ", perimeter_rectange)
    print("Площадь: ", square_rectange)