#!/usr/bin/python3

if __name__ == '__main__':

    ids = {'user1': [213, 213, 213, 15, 213],
        'user2': [54, 54, 119, 119, 119],
        'user3': [213, 98, 98, 35]}

    id = set()

    for item, value in ids.items():
        id = set(value) | id
    
    print(list(id))