#!/usr/bin/python3

def print_tuple():
    i = 1
    colors = ['red', 'orange', 'yellow', 'green', 'cyan', 'blue', 'violet']

    for color in colors:
        print(i, '-th color of rainbow is ', color, sep = '')
        i += 1

if __name__ == '__main__':
    print_tuple()