#!/usr/bin/python3

class A:
    def g(self):
        return 'hello func'

class B:
    arg = 'Python'

    def g(self):
        return self.arg

def main():
    a = A()
    b = A()
    a.arg = 1
    b.arg = 2
    print(a.arg)
    print(b.arg)

    print(a.g())

    b = B()
    print(b.g())

    b.arg = "self"
    print(b.arg)


if __name__ == '__main__':
    main()
