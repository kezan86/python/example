#!/usr/bin/python3

documents = [
    {"type": "passport", "number": "2207 876234", "name": "Василий Гупкин"},
    {"type": "invoice", "number": "11-2", "name": "Геннадий Покемонов"},
    {"type": "insurance", "number": "222", "name": "Аристарх Павлов"}
]

directories = {
    '1': ['2207 876234', '11-2'],
    '2': ['222'],
    '3': []
}

def get_name_by_number():
    number = input('Введите номер документа: ')
    for doc in documents:
        if doc['number'] == number:
            print('{0}'.format(doc['name']))
            break
    else:
        print('Отсутствуют документы с таким номером')
            

def get_directory_by_number():
    number = input('Введите номер документа: ')
    for directory, list_docs in directories.items():
        if number in list_docs:
            print('Документ с номером {0} находится на полке {1}'.format(number, directory))
            break
    else:
        print('Отсутствуют документы с таким номером')

def show_documents():
    for doc in documents:
        print(doc['type'], doc['number'], doc['name'])
    for key, values in directories.items():
        print(key, '->', values)

def add_document():
    number = input('Введите номер документа:')
    name = input('Введите имя и фамилию:')
    doc_type = input('Введите тип документа:')
    directory_number = input('Введите номер полки:')
    if number and name and doc_type and directory_number:
        documents.append({"type": doc_type, "number": number, "name": name})
        if directory_number in directories:
            directories[directory_number].append(number)
        else:
            directories[directory_number] = [number]
    else:
        print('ВНИМАНИЕ! Введены не все данные')

def remove_document():
    person_number = input('Введите номер документа: ')
    bookshelf = ''
    for elem in documents:
        if elem['number'] == person_number:
            documents.remove(elem)
    for number_shelf, number_documents in directories.items():
        if person_number in number_documents:
            number_documents.remove(person_number)
            bookshelf = number_shelf
            break
    print(f'Удален документ с номером: {person_number} из базы данных и убран с полки №{bookshelf}')

def move_documnet():
    number = input('Введите номер документа: ')
    directory_number = input('Введите номер полки: ')

    if number and directory_number:
        if directory_number in directories:
            for directory, list_docs in directories.items():
                if number in list_docs:
                    if directory_number in directories:
                        directories[directory_number].append(number)
                    else:
                        directories[directory_number] = [number]
                    directories[directory].remove(number)
                    print(f'Documet {number} moved in polk {directory_number} and remove in pok {directory}')
                    break
            else:
                print("Введенный документ не существует")
        else:
            print("Указана несуществующая полка")
    else:
        print('ВНИМАНИЕ! Введены не все данные')        


def add_polk():
    number = input('Введите номер новой полки: ')
    for directory in directories.keys():
        if number in directory:
            print("Полка уже существует")
    else:
        directories[number]=[]

def commands(command):
    while True:
        if command == 'p':
            get_name_by_number()
        elif command == 's':
            get_directory_by_number()
        elif command == 'l':
            show_documents()
        elif command == 'a':
            add_document()
        elif command == 'd':
            remove_document()
        elif command == 'm':
            move_documnet()
        elif command == 'as':
            add_polk()
        elif command == 'q':
            print("До встречи!")
            break
        else:
            print("Вы ввели несущестующую команду")
        
        command = input("Введите необходимую команду: ")

def main():
    print("Перечень команд")
    print("p – команда, которая спросит номер документа и выведет имя человека, которому он принадлежит;")
    print("s – команда, которая спросит номер документа и выведет номер полки, на которой он находится;")
    print("l – команда, которая выведет список всех документов в формате passport '2207 876234' 'Василий Гупкин';")
    print("a – команда, которая добавит новый документ в каталог и в перечень полок, спросив его номер, тип, имя владельца и номер полки, на котором он будет храниться.")
    print("d – команда, которая спросит номер документа и удалит его из каталога и из перечня полок")
    print("m – команда, которая спросит номер документа и целевую полку и переместит его с текущей полки на целевую.")
    print("as – команда, которая спросит номер новой полки и добавит ее в перечень")
    print("q - чтобы выйти")

    try:
        command = input("Введите необходимую команду: ")
        commands(command)
    except KeyboardInterrupt:
        print()
        print("Вы вышли из программы нажав сочетание клавиш 'Ctrl+C', возможно это сделано наверенно :)")
        print("До встречи!")

if __name__ == '__main__':
    main()