#!/usr/bin/python3

from datetime import date

if __name__ == '__main__':
    today = date.today()
    current_year = int(today.strftime("%Y"))
    name = input("Введите Ваше имя: ")
    age = int(input("Введите Ваш возраст: "))

    print(f'Меня зовут {name}. Я родился в {current_year-age} году.')