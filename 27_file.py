#!/usr/bin/python3

def main():
    # f = open("27_file.txt")
    # f.write()
    # f.close()

    with open('files/27_file.txt', 'r') as f:
        text = f.read()
        print(text)
        print(type(text))

if __name__ == '__main__':
    main()