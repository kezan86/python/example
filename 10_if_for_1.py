#!/usr/bin/python3

boys = ['Peter', 'Alex', 'John', 'Arthur', 'Richard']
girls = ['Kate', 'Liza', 'Kira', 'Emma', 'Trisha']

if len(boys) != len(girls):
    print("Количество учатников оличается и кто-то отсанеться без пары")
else:
    game = zip(sorted(boys), sorted(girls))
    print("Идеальные пары:")
    for boy, girl in game:
        print(boy, 'и', girl)
    
