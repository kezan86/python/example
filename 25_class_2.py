#!/usr/bin/python3

class Animal():
    def eat(self, food):
        print(food)

class Man(Animal):
    def eat(self, food, cooked=False):
        if cooked:
            print('coocked', food)
        else:
            print(food)

def main():
    cow = Animal()
    cow.eat("Trava")
    man1 = Man()
    man2 = Man()
    man1.eat("Meat")
    man2.eat("Eags", cooked=True)

if __name__ == '__main__':
    main()