#!/usr/bin/python3

def check_age():
    age = int(input("Введите Ваш возраст: "))
    if age <=0:
        print("Введите коректный возраст")
    elif age >= 18:
        print("Отлично, продажа разрешена")
    else:
        print("Извините, вы еще маленький")

if __name__ == '__main__':
    check_age()