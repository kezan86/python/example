#!/usr/bin/python3

def square(number):
    """Square number"""
    return number ** 2

def main():
    num = int(input("Введите число для возведение его в квадрат: "))
    print(square(num))

if __name__ == '__main__':
    main()
