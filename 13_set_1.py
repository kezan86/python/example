#!/usr/bin/python3

if __name__ == '__main__':
    print()

    set_a = {1, 2, 3}
    set_b = {3, 4, 5}

    set_c = set_a | set_b
    print(f'Объединение -> {set_c}')

    set_c = set_a & set_b
    print(f'Пересечение -> {set_c}')

    set_c = set_a - set_b
    print(f'Разность -> {set_c}')

    set_c = set_a ^ set_b
    print(f'Суммирическая разность -> {set_c}')