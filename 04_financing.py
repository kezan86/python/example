#!/usr/bin/python3

if __name__ == '__main__':
    sum = int(input("Введите заработную плату в месяц: "))
    mortgage = int(input("Какой процент(%) от зп уходит на ипотеку: "))
    life = int(input('Какой процент(%) от зп уходит "на жизнь": '))
    sum_mor = sum * mortgage / 100 * 12
    sum_life = sum * life / 100 * 12
    sum_capital = sum * 12 - sum_mor - sum_life
    print()
    print("Вывод:")
    print("На ипотеку было потрачено: ", int(sum_mor), "рублей")
    print("Было накоплено: ", int(sum_capital), "рублей")