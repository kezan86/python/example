# def printThese(a,b,c):
#    print(a, "is stored in a")
#    print(b, "is stored in b")
#    print(c, "is stored in c")
# printThese(1,2,3)

# def printThese(a,b,c):
#    print(a, "is stored in a")
#    print(b, "is stored in b")
#    print(c, "is stored in c")
# printThese(1,2)

# def printThese(a,b,c=None):
#    print(a, "is stored in a")
#    print(b, "is stored in b")
#    print(c, "is stored in c")
# printThese(c=4, a=1, b=2)

def printThese(a=None,b=None,c=None):
   print(a, "is stored in a")
   print(b, "is stored in b")
   print(c, "is stored in c")
printThese(c=3, a=1)