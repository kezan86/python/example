#!/usr/bin/python3

if __name__ == '__main__':
    teams = {'a': 'alpha', 'b': 'bravo'}

    for index, team in teams.items():
        print(team)
    
    print()
    teams['c'] = 'charli'

    for index, team in teams.items():
        print(team)